package selenium;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class seleniumDemo {

    public static void main(String[] args) {
        System.setProperty("webdriver.chrome.driver","src/selenium/chromedriver");
        WebDriver driver = new ChromeDriver();
        driver.get("https://deens-master.now.sh/");


        driver.findElement(By.cssSelector("a[href=\"/login\"]")).click();

        driver.findElement(By.cssSelector("#email")).sendKeys("azat@testpro.io");

            // CSS
            // #email
            //XPATH
            // //*[@id="email"]

        driver.quit();

    }
}